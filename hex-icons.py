# ID: 0cea0c441d1d5e09702bed88163aa7b4d416ab07b34c2e264c814fc7
# Install URL: https://gitlab.com/Ma_124/dynwall2-filters/raw/master/hex-icons.py

def dynwall_filter(cfg, **kwargs):
    try:
        xo = int(cfg["hex-xo"])
    except KeyError:
        xo = 10

    try:
        yo = int(cfg["hex-yo"])
    except KeyError:
        yo = 10

    try:
        xs = cfg["hex-xs"]
    except KeyError:
        xs = 80

    try:
        ys = cfg["hex-ys"]
    except KeyError:
        ys = 73

    for hexDef in cfg["hex-icons"]:
        x = int(hexDef["x"])
        y = int(hexDef["y"])

        x = xo + (xs * x)
        if y % 2 != 0:
            x += int(xs/2)

        y = yo + (ys * y)

        try:
            cfg["icons"]
        except KeyError:
            cfg["icons"] = []

        cfg["icons"].append({
            "x": xo + x,
            "y": yo + y,
            "w": 90,
            "h": 90,
            "img": hexDef["img"],
            "action": hexDef["action"],
        })

    del cfg["hex-icons"]
